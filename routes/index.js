var express = require('express');
var router = express.Router();
var util   = require('util');


router.get('/options', function(req, res, next){
  res.render('./utils/Options', null);
});

// router.get('/iframe', function(req, res, next){
//   res.render('./utils/IFrame', null);
// });

router.get('/iframes/:viewname', function(req, res, next){
  var params = req.query;
  var viewname = req.params.viewname;
  res.render('./iframes/'+viewname, params);
});

router.get('/:foldername/:filename', function(req, res, next) {
  var foldername = req.params.foldername;
  var filename = req.params.filename;
  var viewPath = util.format('pages/%s/%s', foldername, filename);
  res.render('layout', {view:viewPath});
});


/* GET home page. */
router.get('/:filename', function(req, res, next) {
  var filename = req.params.filename;
  var viewPath = 'pages/'+filename;
  res.render('layout', {view:viewPath});
});


module.exports = router;
