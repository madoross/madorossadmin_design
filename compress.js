var path        = require('path')
    ,compressor = require('node-minify')
    ,Q          = require('q');

var compressTargets = [
  {
    input : [
      './public/assets/css/bootstrap.min.css'
      ,'./public/assets/fonts/fonts.googleapis.com.css'
      ,'./public/assets/css/ace.min.css'
      ,'./public/assets/css/datepicker.min.css'
      ,'./public/assets/css/bootstrap-datetimepicker.min.css'      
    ]
    ,output :path.resolve(__dirname, './public/dist', 'base-style.min.css')
  }
  ,
  {
    input : [
      './public/assets/js/ace-extra.min.js'
      ,'./public/assets/js/jquery.2.1.1.min.js'
      ,'./public/assets/js/moment.min.js'
      ,'./public/assets/js/bootstrap.min.js'
      ,'./public/assets/js/ace-elements.min.js'
      ,'./public/assets/js/ace.min.js'
      ,'./public/assets/js/jquery-ui.custom.min.js'
      ,'./public/assets/js/jquery.ui.touch-punch.min.js'
      ,'./public/assets/js/jquery.easypiechart.min.js'
      ,'./public/assets/js/jquery.sparkline.min.js'
      ,'./public/assets/js/jquery.flot.min.js'
      ,'./public/assets/js/jquery.flot.pie.min.js'
      ,'./public/assets/js/jquery.flot.resize.min.js'
      ,'./public/assets/js/jquery.autosize.min.js'
      ,'./public/assets/js/bootstrap-datepicker.min.js'
      ,'./public/assets/js/bootstrap-datetimepicker.min.js'
    ]
    ,output :path.resolve(__dirname, './public/dist', 'base-js.min.js')
  }
];


var promises = [];
for(var i in compressTargets){
  var target = compressTargets[i];
  var promise = compressor.minify({
    compressor: 'no-compress',
    input: target.input,
    output: target.output
  });

  promises.push(promise);
}

Q.all(promises)
.then(
  function(){
    console.log('Theme Files Compressed...');
  }
)
.fail(
  function(err){
    console.error(err);
  }
);
