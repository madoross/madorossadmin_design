if ("undefined" == typeof jQuery) throw new Error("Ace's JavaScript requires jQuery");
jQuery(function(){
  $('.status-select').change(function(e){
    var selectedClass = $(this).find('option:selected').attr('data-parent');
    if(!selectedClass) return false;
    $(this).removeClass('warning').removeClass('success').removeClass('danger');
    $(this).addClass(selectedClass);
  });
  $('textarea[class*=autosize]').autosize({append: "\n"});
});
