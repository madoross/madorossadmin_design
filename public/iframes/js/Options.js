$(function(){

  $('.datepicker').datepicker({format:'yyyy-mm-dd' , autoclose:true})
  .on('changeDate', function(e) {
    var selectedDate = e.date;
    var convertDate = new Date(selectedDate);
    var year = convertDate.getFullYear();
    var month = ((convertDate.getMonth()+1) < 10) ? '0'+(convertDate.getMonth()+1) : (convertDate.getMonth()+1);
    var date  = ((convertDate.getDate()) < 10) ? '0'+(convertDate.getDate()) : (convertDate.getDate());
    var fullDate = year+'-'+month+'-'+date;

    // selectCleaner(-1);
    // selectOptionCreator(0, 'optionChangeListener');

    setSelectOptions(fullDate,tmpData);

  });



});

var holydays = ['2017.06.06','2017.06.10','2017.06.20'];

var tmpData = {
  '1항차': {
    idx : 1,
    start_time_str : '07:00',
    end_time_str : '12:00',
    options : [
      {
        idx : 1,
        name : '대인',
        price : 0
      },
      {
        idx : 2,
        name : '소인',
        price : -10000
      },
      {
        idx : 3,
        name : '여성',
        price : -10000
      },
      {
        idx : 4,
        name : '남성',
        price : 0
      }
    ]
  },
  '2항차' :{
    idx : 2,
    start_time_str : '13:00',
    end_time_str : '18:00',
    options : [
      {
        idx : 5,
        name : '대인',
        price : 0
      },
      {
        idx : 6,
        name : '소인',
        price : -10000
      },
      {
        idx : 7,
        name : '여성',
        price : -10000
      },
      {
        idx : 8,
        name : '남성',
        price : 0
      }
    ]
  }
};


function setSelectOptions(date, datas){
  var html = [];
  html.push('<div class="form-horizontal" role="form" id="selectOptionsArea">');
  html.push('<div class="form-group" data-scope-idx="-1">');
  html.push('<label class="col-sm-3 col-xs-3 control-label">옵션선택</label>');
  html.push('<div class="col-sm-8 col-xs-8" id="optionsArea">');
  html.push('<select class="flat form-control" onchange="onchange_optionSelectListener(event);">');
  html.push('<option value="">::옵션을 선택하세요::</option>');
  for(var key in datas){
    var data = datas[key];
    var timeIdx = data['idx'];
    var startTime = data['start_time_str'];
    var endTime = data['end_time_str'];
    var label = '('+startTime+'~'+endTime+')';
    var fullLabel = key+label;
    var options = data['options'];
    html.push('<option value="" disabled="">------'+key+'------</option>');
    for(var i in options){
      var opt = options[i];
      var optIdx  = opt['idx'];
      var optName = opt['name'];
      var price = opt['price'];
      var text = label + ' '+optName;
      if(price != 0){
        text = text+'&nbsp;&nbsp;&nbsp;&nbsp;'+'('+price+'원)';
      }
      var dataValue = [date,fullLabel,optName];
      dataValue = dataValue.join('/');
      html.push('<option data-date="'+date+'" data-time-idx="'+timeIdx+'" data-opt-idx="'+optIdx+'" data-value="'+dataValue+'" data-price="'+price+'" value="'+dataValue+'">'+text+'</option>');
    }
  }
  html.push('</select>');
  html.push('</div></div></div>');

  $('#selectOptionsArea').html(html.join(''));
}


function onchange_optionSelectListener(e){
  var target = $(e.target);
  var selected = target.find('option:selected');
  var dataDate    = selected.attr('data-date');
  var dataTimeIdx = selected.attr('data-time-idx');
  var dataOptIdx  = selected.attr('data-opt-idx');
  var dataValue   = selected.attr('data-value');
  var dataPrice   = selected.attr('data-price');

  var uqId = dataDate +'-'+ dataTimeIdx +'-'+ dataOptIdx;

  console.log(uqId);
}



function setCounterFromSelected(data){
  var label = data.label;
  var price = data.price;
  var hidden_label = data.hidden_label;
  var id = data.id;

  var container = $('#counterArea');

  var html=
       '<div class="form-group" id="'+id+'">\
        <label class="col-sm-12 col-xs-12 control-label text-left">'+label+'</label>\
        <label class="col-sm-7 col-xs-6 control-label text-right label-strong">권당 ₩'+price+'원</label>\
        <div class="col-sm-3 col-xs-6">\
          <div class="col-sm-10 col-xs-10">\
            <div class="input-group">\
              <input type="hidden" name="opts_text" value="'+hidden_label+'">\
              <span class="input-group-btn info">\
                <button class="btn btn-sm btn-info" type="button" onclick="onclick_btnCounter(\'minus\', event);"><i class="fa fa-minus"></i></button>\
              </span>\
              <input type="number" class="form-control text-center opt-count" data-price="'+price+'" name="opts_count" value="1">\
              <span class="input-group-btn info">\
                <button class="btn btn-sm btn-info" type="button" onclick="onclick_btnCounter(\'plus\', event);"><i class="fa fa-plus"></i></button>\
              </span>\
            </div>\
          </div>\
          <div class="col-sm-2 col-xs-2 no-padding">\
            <button class="btn btn-sm btn-danger btn-remove" type="button" onclick="onclick_removeOptionRow(event);"><i class="fa fa-close"></i></button>\
          </div>\
        </div>\
      </div>';

    if(container.find('#'+id).length > 0){
      var count = Number(container.find('#'+id).find('.opt-count').val());
      container.find('#'+id).find('.opt-count').val(count + 1);
    }else{
      container.append(html);
    }

    onchange_priceChangeListener();

}
