
var mapView = null;
$(function(){
  mapView = new MapView('mapContainer', addressListener);
  mapView.init();

  $("form input[name=address]").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('button.btn-search').click();
            return false;
        } else {
            return true;
        }
    });

});

function addressListener(data){
  var roadAddress  = (data[0].roadAddress.name) ? data[0].roadAddress.name : '';
  var jibunAddress = (data[0].jibunAddress.name) ?  data[0].jibunAddress.name : '';
  var lat = data[0].y;
  var lon = data[0].x;

  $('#addrContainer #jibun').text(jibunAddress);
  if(jibunAddress != ''){
    $('#addrContainer #jibun').attr('onclick', 'parent.mapviewListener("'+jibunAddress+'", '+lat+', '+lon+')');
  }

  $('#addrContainer #road').text(roadAddress);
  if(roadAddress != ''){
    $('#addrContainer #road').attr('onclick', 'parent.mapviewListener("'+roadAddress+'", '+lat+', '+lon+')');
  }
}

var DAUM_API_KEY = 'feee9c5bc8f6b5dac07f5af9b633c56f';

function MapView(containerId, addrListener){
  this.container = document.getElementById(containerId);
  this.lat = this.container.getAttribute('data-lat');
  this.lon = this.container.getAttribute('data-lon');
  this.map_option = {
      center: new daum.maps.LatLng(this.lat, this.lon), // 지도의 중심좌표
      level: 3 // 지도의 확대 레벨
  };
  this.map_width = this.container.offsetWidth;
  this.map_height = this.container.offsetHeight;

  this.position = new daum.maps.LatLng(self.lat, self.lon);
  this.infowindow = new daum.maps.InfoWindow({zindex:1}); // 클릭한 위치에 대한 주소를 표시할 인포윈도우입니다

  this.address_listener = addrListener;
}

MapView.prototype = {
  init : function(){
    var self = this;
    self.map = new daum.maps.Map(this.container, self.map_option); // 지도를 생성합니다

    var mapTypeControl = new daum.maps.MapTypeControl();
    // 지도 확대 축소를 제어할 수 있는  줌 컨트롤을 생성합니다
    var zoomControl = new daum.maps.ZoomControl();

    self.map.relayout();
    self.map.addControl(zoomControl, daum.maps.ControlPosition.RIGHT);
    self.map.addControl(mapTypeControl, daum.maps.ControlPosition.TOPRIGHT);
    self.map.setDraggable(true);
    self.map.setZoomable(true);

    self.geocoder = new daum.maps.services.Geocoder();
    self.marker = new daum.maps.Marker({
      map : self.map
      ,position : self.position
    });

    self.displayMarker(self.map.getCenter());
    console.log(self.lat,'/',self.lon);
    self.map.setCenter(new daum.maps.LatLng(self.lat, self.lon));

    self.setMapClickEventListener(function(data){
      self.displayMarker(data);
    });
  },

  displayMarker : function(location){
    var self = this;
    self.drawMarker(location);
    self.searchDetailAddrFromCoords(location, function(status, result){
      if (status === daum.maps.services.Status.OK) {
          self.address_listener(result);
        }else{
          console.log(result);
        }
    });
  },

  drawInfowindow : function(content){
    this.infowindow.setContent(content);
    this.infowindow.open(this.map, this.marker);
  },

  drawMarker : function(location){
    this.marker.setPosition(location);
    this.marker.setMap(this.map);
  },

  setMapClickEventListener : function(callback){
    daum.maps.event.addListener(this.map, 'click', function(mouseEvent) {
      callback(mouseEvent.latLng);
    });
  },

  searchDetailAddrFromCoords : function(coords, callback) {
      // 좌표로 법정동 상세 주소 정보를 요청합니다
      this.geocoder.coord2detailaddr(coords, callback);
  },
  searchAddrFromCoords : function(coords, callback) {
      // 좌표로 행정동 주소 정보를 요청합니다
      this.geocoder.coord2addr(coords, callback);
  },

  searchAddr2Coord : function(address){
    var self = this;
    this.geocoder.addr2coord(address, function(status, result){
      if (status === daum.maps.services.Status.OK) {
        var moveLatLon = new daum.maps.LatLng(result.addr[0].lat, result.addr[0].lng);
        self.map.setCenter(moveLatLon);
        self.displayMarker(self.map.getCenter());
      }
    });
  }
};
