$(function(){
  $('.datepicker').datepicker({format:'yyyy.mm.dd' , autoclose:true})
  .on('changeDate', function(e) {
    var selectedDate = e.date;
    var convertDate = new Date(selectedDate);
    var year = convertDate.getFullYear();
    var month = ((convertDate.getMonth()+1) < 10) ? '0'+(convertDate.getMonth()+1) : (convertDate.getMonth()+1);
    var date  = ((convertDate.getDate()) < 10) ? '0'+(convertDate.getDate()) : (convertDate.getDate());
    var fullDate = year+'.'+month+'.'+date;

    optionHtml(fullDate, 0, 'optionChangeListener');


  });




});


function generateOptionDatas(){
  var convertedOptions = [];
  var tempOptions = [
    {
      idx : 123,
      names : '갈치낚시(07:00~12:00),참돔낚시(13:00~18:00)',
      is_next : '1,0',
      prices : '0,0'
    },
    {
      idx : 112,
      names : '대인,소인(7세미만)',
      is_next : '1,0',
      prices : '0,-7000'
    }
  ];

  for(var i in tempOptions){
    var opt = tempOptions[i];
    convertedOptions.push(optionSpliter(opt));
  }

  return convertedOptions;
}

function optionHtml(date, index, listenerName){
  var fullOptions = generateOptionDatas();
  console.log(fullOptions);
  var options = fullOptions[index];

  var html = [];
  html.push('<label class="col-sm-3 col-xs-3 control-label" id="">옵션'+(Number(index)+1)+'</label>');
  html.push('<div class="col-sm-8">');
  html.push('<select class="form-control flat" onchange="'+listenerName+'(event)">');
  for(var i in options){
    var option = options[i];
    var next = option.is_next;
    var price = (option.prices != 0) ? '('+option.prices+'원)' : '';
    var value = option.names;
    var text = value + '&nbsp;&nbsp;&nbsp;' + price;
    html.push('<option data-date="'+date+'" data-idx="'+index+'" data-next="'+next+'" data-price="'+price+'" value="'+value+'">'+text+'</option>');
  }

  html.push('</select>');
  html.push('</div>');

  $('#options'+index).html(html.join(''));
}

function optionSpliter(optionsData){
  var splited = [];
  for(var key in optionsData){
    var options = optionsData[key];
    var splitVal = (key != 'idx') ? options.split(',') : [];
    if(splitVal.length > 0){
      for(var i in splitVal){
        if(!splited[i]) splited[i] = {idx:optionsData['idx']};
        splited[i][key] = splitVal[i];
      }
    }
  }
  return splited;
}

function optionChangeListener(e){
  var target = $(e.target);
  var selected = target.find('option:selected');
  var date  = selected.attr('data-date');
  var index = selected.attr('data-idx');
  var next  = selected.attr('data-next');
  var price = selected.attr('data-price');
  var value = selected.val();

  if(!Number(next)) return false;

  index = Number(index) + 1;

  optionHtml(date, index, 'optionChangeListener');
}
