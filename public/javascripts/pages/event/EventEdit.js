$(function(){
  $('.datepicker').datepicker({autoclose:true});
  addInputChangeListener('pcImageUploader', inputChangeListenerCallback);


  addInputChangeListener('mobImageUploader', inputChangeListenerCallback);
});

function uploadTrigger(triggerId){
  $(triggerId).trigger('click');
}

function drawPreviewImage(inputId, previewId, uploadResult){
  $(inputId).attr('data-name', uploadResult.filename);
  $(previewId).css({'background-image':'url(//'+uploadResult.img_src+')'}).parent();
}


/**
 * @description addInputChangeListener inputChangeListener
 * @param {string} inputId
 * @param {function} callback
 */
function addInputChangeListener(inputId, callback){
  document.getElementById(inputId).onchange = function(e) {
    var file = e.target.files[0];
    var filename = this.getAttribute('data-name');
    var preview  = this.getAttribute('data-preview');
    var self     = '#'+this.getAttribute('id');
    if (file) {
      if (/^image\//i.test(file.type)) {
        var data = {file:file, filename:filename, preview:preview, self:self};
        readFile(data, callback);
      } else {
        alert('이미지 형식의 파일만 가능합니다.');
      }
    }
  }
}


/**
 * @description inputChangeListenerCallback inputChangeListenerCallback
 * @param {string} inputId
 * @param {function} callback
 */
function inputChangeListenerCallback(data){
  var params = {
      base64 : data.base64,
      bucketname : 'madoross-prep',
      filename : data.filename || null
    };
  asyncUploadImage(params, function(result){
    console.log(result);
    drawPreviewImage(data.self, data.preview, result);
  });
}

/**
 * @description input file reader
 * @param {object} data
 * @param {function} callback
 */
function readFile(data, callback) {
	var reader = new FileReader();

	reader.onloadend = function () {
    processImage(reader.result, data, callback);
	}

	reader.onerror = function () {
		alert('There was an error reading the file!');
	}

	reader.readAsDataURL(data.file);
}


/**
 * @description Convert Image To base64
 * @param {object} dataURL
 * @param {object} data
 * @param {function} callback
 */
function processImage(dataURL, data, callback){
  var image        = new Image();
  var fileType     = data.file.fileType;
  var originFilename = data.file.name;
  var extension =  (/[.]/.exec(originFilename)) ? /[^.]+$/.exec(originFilename) : 'jpeg';
  var mimeType = 'image/'+extension;
  image.src = dataURL;
  image.onload = function(){
    var canvas    = document.createElement('canvas');
    canvas.width  = this.width;
    canvas.height = this.height;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(this, 0, 0);
    var base64 = canvas.toDataURL(mimeType);
    var result = {
      base64 : base64,
      filename : (data.filename && data.filename != '') ? data.filename : null,
      preview : data.preview,
      self : data.self,
      width : this.width,
      height : this.height
    };
    callback(result);
  }
}

function asyncUploadImage(data, success, error){
  var ajaxOptions = {
		type: 'POST',
		data: data,
		success: success
	};

  // ajaxOptions.url = 'http://imageprep.mogit.xyz/s3/upload/base64';
  ajaxOptions.url = 'http://localhost:8888/s3/upload/base64';

  if(error){
    ajaxOptions.error = error;
  }else{
    ajaxOptions.error = function (err) {
      console.log(err);
      alert('업로드 에러');
		};
  }

  console.log(ajaxOptions);

  $.ajax(ajaxOptions);
}
