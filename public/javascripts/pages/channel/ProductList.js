$(function(){
  setColumnControllerEvent('#productTable');
});


function setColumnControllerEvent(tableId){
  var tableTarget = $(tableId);
  $('.column-controller input[type=checkbox]').click(function(){
    var checkbox = $(this);
    var columnIdx = checkbox.val();
    console.log(columnIdx, '/', checkbox.is(':checked'));
    if(checkbox.is(':checked')){
      tableTarget.find('.col-idx-'+columnIdx).show();
    }else{
      tableTarget.find('.col-idx-'+columnIdx).hide();
    }
  });
}
