$(function(){
  $('input.search-input').focusin(function(){
    var input = $(this);
    input.closest('.input-group').find('.dropdown').addClass('open');
  });

  $('input.search-input').focusout(function(){
    var input = $(this);
    input.closest('.input-group').find('.dropdown').removeClass('open');
  });

});

function mapviewListener(addr, lat, lon){
  $('input[name=address]').val(addr);
  $('input[name=latlon]').val(lat+','+lon);
  $('#addressfinder').modal('hide');
  $('#addressfinder iframe').attr('src', '/iframes/Mapview?lat='+lat+'&lon='+lon);
}

function openAddressFinder(lat, lon){
  $('#addressfinder').modal('show');
  $('#addressfinder iframe').attr('src', '/iframes/Mapview?lat='+lat+'&lon='+lon);
}
