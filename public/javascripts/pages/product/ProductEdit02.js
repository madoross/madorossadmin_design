$(function(){
  $('.datepicker').datepicker({autoclose:true});
  $('.datetimepicker').datetimepicker({format:'HH:mm',pickDate: false});

  $(':input[type=number]' ).on('mousewheel',function(e){ $(this).blur(); });

});


function onclick_btnOptionEdit(form){
  form = $(form);
  form.find('.row').each(function(){
    var row = $(this);
    if(row.hasClass('hidden')){
      row.removeClass('hidden');
    }else{
      row.addClass('hidden');
    }
  });
}

function onclick_btnOptionSave(form){
  form = $(form);
  console.log(form);
  form.find('.row').each(function(){
    console.log('row');
    var row = $(this);
    if(row.hasClass('hidden')){
      row.removeClass('hidden');
    }else{
      row.addClass('hidden');
    }
  });
}
