$(function(){
  $('#drag-sortable').sortable(
    {
      revert:true,
      stop : function(evt, ui){
        $('#drag-sortable li').each(function(index){
          $(this).find('.sort-no').text((index+1));
        });
      }
    }
  );
});
