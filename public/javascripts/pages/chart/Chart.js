var presets = window.chartColors;
var utils = Samples.utils;
var inputs = {
	min: -100,
	max: 100,
	count: 8,
	decimals: 2,
	continuity: 1
};

function generateData(config) {
	return utils.numbers(utils.merge(inputs, config || {}));
}

function generateLabels(config) {
	return utils.months(utils.merge({
		count: inputs.count,
		section: 3
	}, config || {}));
}

var datas = {"Jan":1.24,"Feb":31.59,"Mar":74.39,"Apr":-39.95,"May":-98.32,"Jun":-47.84,"Jul":29.26,"Aug":55.85};

function ChartGenerator(container, dataSet){

  var options = {
  	maintainAspectRatio: false,
  	spanGaps: false,
  	elements: {
  		line: {
  			tension: 0.000001
  		}
  	},
  	plugins: {
  		filler: {
  			propagate: false
  		}
  	},
  	scales: {
  		xAxes: [{
  			ticks: {
  				autoSkip: false,
  				maxRotation: 0
  			}
  		}]
  	}
  };


  new Chart(container, {
		type: 'line',
		data: {
			labels: Object.keys(dataSet),
			datasets: [{
				backgroundColor: utils.transparentize(presets.red),
				borderColor: presets.red,
				data: Object.values(dataSet),
				label: '판매건수',
				fill: false
			}]
		},
		options: utils.merge(options, {})
	});


  Chart.plugins.register({
           afterDatasetsDraw: function(chart, easing) {
               // To only draw at the end of animation, check for easing === 1
               var ctx = chart.ctx;

               chart.data.datasets.forEach(function (dataset, i) {
                   var meta = chart.getDatasetMeta(i);
                   if (!meta.hidden) {
                       meta.data.forEach(function(element, index) {
                           // Draw the text in black, with the specified font
                           ctx.fillStyle = 'rgb(0, 0, 0)';

                           var fontSize = 16;
                           var fontStyle = 'normal';
                           var fontFamily = 'Helvetica Neue';
                           ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                           // Just naively convert to string for now
                           var dataString = dataset.data[index].toString();

                           // Make sure alignment settings are correct
                           ctx.textAlign = 'center';
                           ctx.textBaseline = 'middle';

                           var padding = 5;
                           var position = element.tooltipPosition();
                           ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                       });
                   }
               });
             }
           });
}
//
// {
//   title: {
//     text: '일별 판매수량',
//     display: true
//   }
// }
